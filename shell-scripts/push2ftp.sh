#!/bin/bash

function checkRequirements {
  local provideString='You have to provide'
  local properUsage='Proper usage: push2ftp <user> <password> <host> <syncroot>'

	# check for username
	if [ -z "$1" ]
		then
			echo "$provideString an username."
			echo $properUsage
			exit 2
	fi

	# check for password
	if [ -z "$2" ]
		then
      echo "$provideString a password."
			echo $properUsage
			exit 2
	fi

	# check for host
	if [ -z "$3" ]
		then
      echo "$provideString a host."
			echo $properUsage
			exit 2
	fi

	# check for syncroot
	if [ -z "$4" ]
		then
			echo "$provideString a syncroot."
			echo $properUsage
			exit 2
	fi
}

function configure {
	# set git-ftp config
	git config git-ftp.user "$1"
	git config git-ftp.password "$2"
	git config git-ftp.url "$3"
	git config git-ftp.syncroot "$4"
	git config git-ftp.insecure 1
  	# git config git-ftp.key .ssh/id_rsa

	git stash save -q
}

function gitFtpPush {
	# git ftp push to ftp
	git ftp push -v 2>/dev/null
	gitpushErrorCode="$?"
}

function gitFtpInit {
	# git ftp init on ftp
	git ftp init -v 2>/dev/null
	gitinitErrorCode="$?"
}

checkRequirements $1 $2 $3 $4
configure $1 $2 $3 $4
gitFtpPush

# check if git ftp push failed
if [ "$gitpushErrorCode" -ne 0 ]
	then
		echo -e "\e[31m\"git ftp push\" failed.\e[0m"
		gitFtpInit
		# check if git init failed
		if [ "$gitinitErrorCode" -ne 0 ]
			then
				# git ftp init failed
				echo -e "\e[31m\"git init\" failed.\e[0m"
				exit 3
			else
				# git ftp init succeeded
				echo -e "\e[92m\"git ftp init\" finished successfully.\e[0m"
				exit 0
		fi
	else
		# git ftp push succeeded
		echo -e "\e[92m\"git ftp push\" finished successfully.\e[0m"
		exit 0
fi

# should not be reached
echo -e "\e[31mSomething unexspected happend...\e[0m"
exit 1