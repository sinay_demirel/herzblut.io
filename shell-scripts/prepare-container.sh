#!/bin/bash

# do updates
apt-get -qq update

# check for git
# install it if not available
if [ $(dpkg-query -W -f='${Status}' git-core 2>/dev/null | grep -c 'ok installed') -eq 0 ]
	then
		apt-get install -y -qq git-core
fi

# check for git-ftp
# install it if not available
if [ $(dpkg-query -W -f='${Status}' git-ftp 2>/dev/null | grep -c 'ok installed') -eq 0 ]
	then
		apt-get install -y -qq git-ftp
fi

# set git config
git config --global user.email "status@neosmart.de"
git config --global user.name "Bitbucket Server"