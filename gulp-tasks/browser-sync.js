module.exports = function (gulp, plugins, globals) {
	return function () {
		plugins.browserSync = plugins.browserSync.create();
	    plugins.browserSync.init({
	        // External Server
	        proxy: globals.config.browserSync.proxy,
	        notify: globals.config.browserSync.notify
	    });
	};
};