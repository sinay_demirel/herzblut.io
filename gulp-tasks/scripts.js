module.exports = function (gulp, plugins, globals) {
	return function () {
		gulp.src(globals.srcJS)
			.pipe(plugins.concat('frontend.js'))
			.pipe(plugins.uglify())
			.pipe(gulp.dest(globals.destJS));
	};
};