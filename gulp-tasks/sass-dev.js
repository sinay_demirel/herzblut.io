var path = require('path');

module.exports = function (gulp, plugins, globals) {
	return function () {
		gulp.src(globals.srcSASS)
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.sass({
				includePaths: [
					path.join(globals.paths.nodeModules, 'include-media', 'dist')
				],
				errLogConsole: true
			}))
			.pipe(plugins.autoprefixer())
			.pipe(plugins.rename({basename: 'style'}))
			.pipe(plugins.sourcemaps.write()) 
			.pipe(gulp.dest(globals.destSASS))
			.pipe(plugins.browserSync.stream({match: '**/*.css'}));
	};
};