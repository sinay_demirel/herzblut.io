module.exports = function (gulp, plugins, globals) {
	return function () {
		gulp.src(globals.srcSASS)
			.pipe(plugins.sass())
			.pipe(plugins.autoprefixer())
			.pipe(plugins.uglifycss())
			.pipe(plugins.rename({basename: 'style'})) 
			.pipe(gulp.dest(globals.destSASS))
			.pipe(plugins.browserSync.stream({match: '**/*.css'}));
	};
};