<?php

if (!defined('NEOM_FUNCTIONS_PATH')) {
	define('NEOM_FUNCTIONS_PATH', get_template_directory().'/functions');
}

if (!defined('NEOM_FILTER_PATH')) {
	define('NEOM_FILTER_PATH', NEOM_FUNCTIONS_PATH.'/filter');
}

if (!defined('NEOM_SHORTCODE_PATH')) {
	define('NEOM_SHORTCODE_PATH', NEOM_FUNCTIONS_PATH.'/shortcodes');
}

if (!defined('NEOM_ACF_PATH')) {
	define('NEOM_ACF_PATH', NEOM_FUNCTIONS_PATH.'/acf');
}

if (!defined('NEOM_JQUERY_URI')) {
	define('NEOM_JQUERY_URI', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
}