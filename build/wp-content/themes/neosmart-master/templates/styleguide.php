<?php
/**
 * Template Name: Styleguide
 *
 * Have a look at this pages:
 * 		http://styleguides.io/examples.html
 * 			http://codepen.io/guide/
 * 			https://material.io/guidelines/material-design/introduction.html#
 */

get_header();

?>
<header class="stage">
	<h1>Eine wunderbare Heiterkeit hat meine ganze Seele eingenommen.</h1>
</header>

<o>Gleich den süßen Frühlingsmorgen, die ich mit ganzem Herzen genieße. Ich bin allein und freue mich meines Lebens in dieser Gegend, die für solche Seelen geschaffen ist wie die meine. Ich bin so glücklich, mein Bester, so ganz in dem Gefühle von ruhigem Dasein versunken, daß meine Kunst darunter leidet. Ich könnte jetzt nicht zeichnen, nicht einen Strich.</p>
<h2>Ich dann im hohen Grase am fallenden Bache liege</h2>
und näher an der Erde tausend mannigfaltige Gräschen mir merkwürdig werden; wenn ich das Wimmeln der kleinen Welt zwischen Halmen, die unzähligen, unergründlichen Gestalten der Würmchen, der Mückchen näher an meinem Herzen fühle, und fühle die Gegenwart des Allmächtigen, der uns nach seinem Bilde schuf, das Wehen des Alliebenden.
<h3>Mein Freund - aber ich gehe darüber zugrunde</h3>
Ich erliege unter der Gewalt der Herrlichkeit dieser Erscheinungen. Eine wunderbare Heiterkeit hat meine ganze Seele eingenommen, gleich den süßen Frühlingsmorgen, die ich mit ganzem Herzen genieße. Ich bin allein und freue mich meines Lebens in dieser Gegend, die für solche Seelen geschaffen ist wie die meine. Ich bin so glücklich, mein Bester.
<h4>Als in diesen Augenblicken. Wenn das liebe Tal um mich dampft</h4>
<ol>
 	<li>Ordered List Item</li>
 	<li>Ordered List Item</li>
 	<li>Ordered List Item</li>
 	<li>Ordered List Item</li>
</ol>
<h5>Der uns in ewiger Wonne schwebend trägt und erhält; mein Freund!</h5>
<ul>
 	<li>Unordered List Item</li>
 	<li>Unordered List Item</li>
 	<li>Unordered List Item</li>
 	<li>Unordered List Item</li>
</ul>
<h6>Ich erliege unter der Gewalt der Herrlichkeit dieser Erscheinungen.</h6>
Eine wunderbare Heiterkeit hat meine ganze Seele eingenommen, gleich den süßen Frühlingsmorgen, die ich mit ganzem Herzen genieße. Ich bin allein und freue mich meines Lebens in dieser Gegend, die für solche Seelen geschaffen ist wie die meine. Ich bin so glücklich, mein Bester, so ganz in dem Gefühle von ruhigem Dasein versunken, daß meine Kunst darunter leidet.
<blockquote>Ich könnte jetzt nicht zeichnen, nicht einen Strich, und bin nie ein größerer Maler gewesen als in diesen Augenblicken. Wenn das liebe Tal um mich dampft, und die hohe Sonne an der Oberfläche der undurchdringlichen Finsternis meines Waldes ruht, und nur einzelne Strahlen sich in das innere Heiligtum stehlen, ich dann im hohen Grase am fallenden Bache liege.</blockquote>

<h3>Formular</h3>
<form>
<input type="text">
<button>Senden</button>
</form>
<?php get_footer(); ?>