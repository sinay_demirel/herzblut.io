<?php

if (!defined('NEO_FUNCTIONS_PATH')) {
	define('NEO_FUNCTIONS_PATH', get_template_directory().'/includes/');
}


require_once(NEO_FUNCTIONS_PATH.'filter.php');
require_once(NEO_FUNCTIONS_PATH.'actions.php');
//require_once(NEO_FUNCTIONS_PATH.'shortcodes.php');
//require_once(NEO_FUNCTIONS_PATH.'acf.php');