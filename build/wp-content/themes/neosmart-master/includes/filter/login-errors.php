<?php

function neom_login_error_message($error){
    global $errors;
	$err_codes = $errors->get_error_codes();

	$errorString = __('Das Passwort und oder der Benutzername, den du eingegeben hast, sind nicht korrekt.', 'neosmart');
	$passwordLostString = __('Passwort vergessen?', 'neosmart');

	return sprintf('%s <a href="%s">%s</a>', $errorString, wp_lostpassword_url(), $passwordLostString);
}

add_filter('login_errors','neom_login_error_message');