<?php

// remove version number from generator
function remove_version_generator() {
    return '';
}
add_filter('the_generator', 'remove_version_generator');
 
// remove version number from scripts and styles
function remove_version_from_style_js( $src ) {
    if (strpos($src, sprintf('ver=%s', get_bloginfo('version')))) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter('style_loader_src', 'remove_version_from_style_js');
add_filter('script_loader_src', 'remove_version_from_style_js'); 