<?php

// Enable style select dropdown
function neom_mce_editor_buttons($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'neom_mce_editor_buttons');

// Add custom formats
function neom_mce_before_init($settings) {
    $style_formats = array(
    	// Lead
        array(
	        'title' => 'Lead',
	        'selector' => 'p',
	        'classes' => 'lead',
            'wrapper' => true,
            'styles' => array(
                'fontSize' => '1.2rem'
            )
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;
}
add_filter('tiny_mce_before_init', 'neom_mce_before_init');