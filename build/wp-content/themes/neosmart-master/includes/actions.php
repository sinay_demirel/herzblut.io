<?php

/**
 * Includes
 */

require_once('actions/backend-scripts.php');
require_once('actions/frontend-scripts.php');
require_once('actions/styles.php');
require_once('actions/image-sizes.php');

/**
 * Setup theme
 */
add_action('init', function() {
    add_theme_support( 'post-thumbnails' );
    add_post_type_support( 'page', 'excerpt' );
});

/***********************************************************************************************
 * Disable WP Logo in Admin Area
 **/

add_action('admin_bar_menu', function ( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}, 999 );