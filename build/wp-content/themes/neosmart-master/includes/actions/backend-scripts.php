<?php

/***********************************************************************************************
 * Include Admin JS
 **/

add_action('admin_enqueue_scripts', function(){
	wp_enqueue_script('amadeus-backend-scripts', get_template_directory_uri() . '/js/backend.js', array('jquery'), '1.0.0', true);
});