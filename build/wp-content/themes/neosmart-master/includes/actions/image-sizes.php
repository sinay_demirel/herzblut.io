<?php

/**************************************************************************
 * Generate Image sizes indepent from position in theme
 * Orientaion by common screen resolutions
 *
 * Media-Settings [width,height,crop]
 * thumbnail 	> 120,120,true
 * medium 		> 480,480,false
 * large 		> 1024,1024,false
 **/

if (!function_exists('neo_add_image_sizes')) {
	function neo_add_image_sizes()  {
       
		//add_image_size('size-160', 160, 160);
		//add_image_size('size-240', 240, 240);
		add_image_size('size-320', 320, 320);
		//add_image_size('size-360', 360, 360);
		//add_image_size('size-480', 480, 480);
		add_image_size('size-640', 640, 640);
		//add_image_size('size-720', 720, 720);
		add_image_size('size-768', 768, 768);
		add_image_size('size-960', 960, 960);
		//add_image_size('size-1024', 1024, 1024);
		add_image_size('size-1280', 1280, 1280);
		//add_image_size('size-1360', 1360, 1360);
		add_image_size('size-1366', 1366, 1366);
		add_image_size('size-1440', 1440, 1440);
		//add_image_size('size-1680', 1680, 1680);
		add_image_size('size-1920', 1920, 1920);

	}
	add_action('after_setup_theme', 'neo_add_image_sizes');
}