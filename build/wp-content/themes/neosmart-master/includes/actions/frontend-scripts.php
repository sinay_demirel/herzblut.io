<?php

add_action("wp_enqueue_scripts", function () {

	// If Frontend > move jQuery to footer
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_dequeue_script('jquery');
		wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', [], false, true);
	}

	// Master JS
	wp_enqueue_script('neosmart-master', get_template_directory_uri() . '/js/frontend.js', array('jquery'), '1.0.0', true);

	// Localizes a registered script with data for a JavaScript variable.
    wp_localize_script( 'neosmart-master', 'neosmart', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

});