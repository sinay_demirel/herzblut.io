<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// Configs
switch($_SERVER['SERVER_NAME']) {
	// Daniel Benkenstein
    case 'neosmart-master.db.io':
    	require_once ('wp-configs/db.php');
        break;
    // Florian Vogt
    case 'neosmart-master.fv.io':
    	require_once ('wp-configs/fv.php');
        break;
    // Christian Horsch
    case 'neosmart-master.ch.io':
    	require_once ('wp-configs/ch.php');
        break;
    // Sinay Demirel
    case 'neosmart-master.sd.io':
    	require_once ('wp-configs/sd.php');
        break;
    // Develop@DasDingX
    case 'neosmart-master.develop.neosmart.biz':
    	require_once ('wp-configs/develop@DasDingX.php');
        break;
    default:
        die('Für diesen Server wurde noch keine Konfiguration angegeben.');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// ** WordPress Home && WordPress Siteurl dynamisch setzen ** //
$tmp_server_protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
define('WP_HOME',           $tmp_server_protocol.$_SERVER['SERVER_NAME'].'/');
define('WP_SITEURL',        $tmp_server_protocol.$_SERVER['SERVER_NAME'].'/');

// Security
define('DISALLOW_FILE_EDIT', true);

// MISC
define('WP_AUTO_UPDATE_CORE', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kU*?mY|rY*S7KrG,#ak% fDYdgd&xV[ONg9HZS5R[&wu>o9:&@^cy-]YW![,3v`C');
define('SECURE_AUTH_KEY',  '5vIEE)=41e3cc<aQkd}7c`y+<x1$SaCRm=,+ayhM/!%lL< ^bf8tOx-?{ez{<e2{');
define('LOGGED_IN_KEY',    '3w]]V9H~jNpaAGjrxP!DFFC0=/+h4_1kzg6p=%fD<|sot]KoRpWoZ@#`7iTtXRww');
define('NONCE_KEY',        '17+~K(=p:ff8sh$(bH^= Cd/.vdlOux1bNM[YdLSP>AS#,Ug w4-(@:L KjYZwXu');
define('AUTH_SALT',        'ETi 2c9^;.I59`xAc|3qVRi0^>9|`5)OrgB3oG|C;n Ud2yH)E3bZY(Hy-35]dh3');
define('SECURE_AUTH_SALT', 'hfkC$q73P7L8X kbI7-gsy5mi5tcHJ#hV[F&fhRyreK&`<5wvrcy[AU:C&Hm S?Z');
define('LOGGED_IN_SALT',   '?fd.uwt7T/R%Mj=?`A50bQDKrhY2$YAnDyvA6Hwf,:R{9.ns~+jJ_^])+L@Wburm');
define('NONCE_SALT',       'PCn>*%4/A$+~{crslCzsOL4U j*ckTmBFhbg&qN[Z=a>L<ws{e$ka9?4Nn,1v@HK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'neo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
