//Import modules
var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')(),
	path = require('path');

plugins.del = require('del');
plugins.browserSync = require('browser-sync');

//Global Vars
var globals = {
	srcJS: 		['./src/neosmart-master/js/frontend/**/*.js'],
	srcSASS: 	['./src/neosmart-master/sass/**/*.{sass,scss}','./widgets/**/*.{sass,scss}'],
	destJS: 	path.join(__dirname,'build','wp-content','themes','neosmart-master','js'),
	destSASS: 	path.join(__dirname,'build','wp-content','themes','neosmart-master'),
	config: 	require('./gulp-config.json'),
	paths: {
		nodeModules: path.join(__dirname, 'node_modules')
	}
}

//Load Tasks
gulp.task('scripts', getTask('scripts'));
gulp.task('sass', getTask('sass'));
gulp.task('sass-dev', getTask('sass-dev'));
gulp.task('clean', getTask('clean'));
gulp.task('browser-sync', getTask('browser-sync'));

//Default Task
gulp.task('default', ['browser-sync','scripts', 'sass-dev'], function () {
	gulp.watch(globals.srcJS, ['scripts']).on('change', plugins.browserSync.reload);
	gulp.watch(globals.srcSASS, ['sass-dev']);
	//gulp.watch('./**/*.php').on('change', plugins.browserSync.reload);
	gulp.watch('./widgets/**/*.js').on('change', plugins.browserSync.reload);
});

//Get Tasks
function getTask(task) {
	return require('./gulp-tasks/' + task)(gulp, plugins, globals);
}